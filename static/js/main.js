require.config({
    baseUrl: 'static/js',
    urlArgs: "bust=" +  (new Date()).getTime(),
    paths: {
        // Libs
        jquery: 'libs/jquery',
        underscore: 'libs/underscore',
        backbone: 'libs/backbone',
        bootstrap: 'libs/bootstrap',
        
        // App
        app: 'app/app',
        router: 'app/router',
        
        // Backbone directory definitions
        models: 'app/models',
        collections: 'app/collections',
        views: 'app/views'

    },
    shim: {
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        }
    }
});
        
require([
    'app'
], function(App) {
    App.initialize();
});
